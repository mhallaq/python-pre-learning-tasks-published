def vowel_swapper(string):
    # ==============
    # Your code here
    vowel_dictionary = {"a": "4", "A": "4", "e": "3", "E": "3", "i": "!", "I": "!", "o": "ooo", "O": "000",
                        "u": "|_|", "U": "|_|"}
    for char in string:
        if char in vowel_dictionary:
            result_string = string.replace(char, vowel_dictionary[char])

    return result_string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
