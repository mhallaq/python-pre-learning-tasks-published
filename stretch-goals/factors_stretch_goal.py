def factors(number):
    # ==============
    # Your code here

    i=2
    result_array=[]
    while i<number:
        result=number/i
        if result.is_integer():
            result_array.append(i)
        i=i+1
    if len(result_array)>0:
        return result_array
    else:
        return str(number)+" is a prime number"
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
